import axios from 'axios';

class GeneralService {

  getServices() {
    return axios.get(process.env.REACT_APP_API_URL + 'services');
  }

  getReviews() {
    return axios.get(process.env.REACT_APP_API_URL + 'reviews');
  }

}

export default new GeneralService();
