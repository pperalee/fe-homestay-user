import axios from 'axios';
import authHeader from './auth-header';

class UserService {

  getMe() {
    return axios.get(process.env.REACT_APP_API_URL + 'me', { headers: authHeader() });
  }

  getHistory() {
    return axios.get(process.env.REACT_APP_API_URL + 'user/bookings', { headers: authHeader() });
  }

  updateMe(name, address, phone) {
    return axios.patch(process.env.REACT_APP_API_URL + "profile", {
      name: name,
      phone_number: phone,
      address: address
    }, {headers: authHeader()});
  }

  updatePassword(oldPassword, newPassword) {
    return axios.patch(process.env.REACT_APP_API_URL + "password/change", {
      password: oldPassword,
      new_password: newPassword
    }, {headers: authHeader()});
  }
}

export default new UserService();
