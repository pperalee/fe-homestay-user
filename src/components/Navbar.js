import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import AuthService from '../services/auth.service';
import Button from './shared/Button';
import {AuthContext} from '../auth/Auth';

const USER_ROLES = {
  USER: 1,
  ADMIN: 2,
}

class Navbar extends Component {
  render() {
    return (
      <header className="sticky-dark-bg">
        <div className="header-area">
          <div className="container">
            <div className="row">
              <div className="col-sm-2">
                <div className="logo">
                  <a href="/"> JadeHill<span>Homestay</span></a>
                </div>
              </div>
              <div className="col-sm-10">
                <div className="main-menu">

                  <div className="navbar-header">
                    <button type="button" className="navbar-toggle"
                            data-toggle="collapse"
                            data-target=".navbar-collapse">
                      <i className="fa fa-bars"/></button>
                  </div>

                  <div className="collapse navbar-collapse">
                    <ul className="nav navbar-nav navbar-right">

                      {/* {user ? (
                        <React.Fragment>
                          <li>
                            <a className="button-link" href="/#book">
                              <Button>Đặt phòng</Button>
                            </a>
                          </li>
                          <li>
                            <a href="/info">Chào {user.username}</a>
                          </li>
                          {(user.roles === USER_ROLES.ADMIN) && (
                            <li>
                              <a href="/admin">Quản lý</a>
                            </li>
                          )}
                          <li>
                            <a href="/" onClick={e => this.handleLogOut(e)}>
                              Đăng xuất
                            </a>
                          </li>
                        </React.Fragment> */}
                      {/* ) : ( */}
                        <React.Fragment>
                          <li>
                            <a href="/">Giới thiệu</a>
                          </li>
                          <li>
                            <a href="/#explore">Xem Homestay</a>
                          </li>
                          <li>
                            <a className="button-link" href="/#explore">
                              <Button>Đặt phòng</Button>
                            </a>
                          </li>
                        </React.Fragment>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div className="home-border"/>
          </div>
        </div>
      </header>
    );
  }
}

Navbar.contextType = AuthContext;

export {Navbar};
