import React, {Component} from 'react';
import About from './About';
import { DestinationFilter } from '../home/DestinationFilter';

export default class Home extends Component {
  

  render() {
    return (
      <React.Fragment>
        <About/>
        <DestinationFilter/>
      </React.Fragment>
    )
  }
}
