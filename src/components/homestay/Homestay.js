import React, { useState, useEffect } from "react";
import SlideShow from "./SlideShow";
import "./Homestay.css";
import defaultRoom  from "./images/defaultRoom.jpg"
import { Modal } from 'react-bootstrap';
import axios from "axios";

const url = process.env.REACT_APP_API_URL

const rooms = [
    "room1",
    "room2",
    "room3",
    "room4",
    "room5",
    "room6",
    "room7",
    "room8",
    "room9",
];
const services = ["service1", "service2", "service3"];

const Homestay = ({ homestay, show, onHide }) => {
    // const [hs, setHs] = useState(null)
    const [allRooms, setAllRooms] = useState([])
    const [allServices, setAllServices] = useState([])

    useEffect(() => {
        if (!homestay) return;
        const getAllRooms = async () => {
            const response = await axios({
                method: "get",
                url: process.env.REACT_APP_API_URL + `homestays/${homestay._id}/roomTypes`,
                responseType: "json",
                // headers: {
                //     Authorization: `Bearer ${token}`,
                // },
            });
            const roomList = await response.data;
            return roomList;
        };
        const getAllServices = async () => {
            const response = await axios({
                method: "get",
                url: process.env.REACT_APP_API_URL + `search/homestays/${homestay._id}/`,
                responseType: "json",
                // headers: {
                //     Authorization: `Bearer ${token}`,
                // },
            });
            const servicesList = await response.data;
            return servicesList
            // return roomList;
        };

        const getInfo = async () => {
            const all_rooms = await getAllRooms();
            const all_services = await getAllServices()
            // console.log(all_rooms)
            setAllRooms(all_rooms)
            setAllServices(all_services.services)
        }
        getInfo()
        // const all_services =  getAllServices();

        // setAllServices(all_services)
    }, [homestay]);

    if (!homestay) return <p>Loading...</p>
    console.log(homestay)
    console.log(allRooms)

    return (
        <Modal show={show} bsSize="lg" onHide={onHide}>
            <Modal.Header closeButton>
                {/* <Modal.Title>{props.homestay.name}</Modal.Title> */}
            </Modal.Header>
            <Modal.Body>
                <div className="user-hs-container">
                    <div className="user-hs-container-2">
                        <div className="user-hs-info">
                            <div className="user-hs-content">
                                <p className="user-hs-content-title">{homestay.name}</p>
                                <div className="user-hs-content-text">
                                    <p>Địa chỉ: {homestay.address}</p>
                                    <p>
                                        {homestay.description}
                                    </p>
                                    <p>Gọi ngay: {homestay.phone}</p>
                                </div>
                                {/* <button>Đặt ngay</button> */}
                            </div>
                        </div>
                        <div className="user-hs-images">
                            <SlideShow imgs={homestay.images} />
                        </div>
                        <div className="user-hs-rooms-container">
                            <h4>ROOMS</h4>
                            <div className="user-hs-rooms">
                                {allRooms.map((item, index) => (
                                    <div key={index} className="user-hs-rooms-item">
                                        {item.image === undefined ? <img src={defaultRoom} alt="img" />
                                            :
                                            <img src={`${url}search/roomTypes/${item._id}/image`} alt="anh" />}
                                        <div className="user-hs-rooms-item-content">
                                            <p style={{ fontSize: 20 }}><b>{item.name}</b></p>
                                            {/* <p><i>{item.description}</i></p> */}
                                            {/* <br></br> */}
                                            <div style={{
                                                display: "flex", flexDirection: "column", justifyContent: "flex-start",
                                                // borderTopRightRadius: "5px", borderBottomRightRadius: "5px"
                                            }}>
                                                <p>Số lượng khách: {item.min_people} - {item.max_people}</p>
                                                <p>Số giường: {item.single_bed} giường đơn, {item.multi_bed} giường đôi</p>
                                                <p>Giá: {item.price}/đêm</p>
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                        <div className="user-hs-services-container">
                            <h4>Services</h4>
                            <div className="user-hs-services">
                                {allServices.map((item, index) => (
                                    <div key={index} className="user-hs-services-item">
                                        <p>{item.name}</p>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
            </Modal.Body>
        </Modal>
    );
};

export default Homestay;