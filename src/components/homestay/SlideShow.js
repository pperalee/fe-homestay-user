import React, { useState, useEffect } from "react";
import "./SlideShow.css";
import example1 from "./images/example1.jpg"
import example2 from "./images/example2.jpg"
import example3 from "./images/example3.jpg"

const decodeBase64toImg = (base64string, index) => {
    let src = "data:image/jpeg;base64,"
    src += base64string;
    return src;
    // var newImage = document.createElement('img')
    // newImage.src = src
    // newImage.width = newImage.height = "200"
    // document.getElementById(`#imageContainer${index}`).innerHTML = newImage.outerHTML
}


const SlideShow = ({ imgs }) => {
    const [images, setImages] = useState([example1, example2, example3])

    useEffect(() => {
        if (imgs[0] === 'image') {
            return
        }
        else {
            let tmp = []
            for (let i = 0; i < imgs.length; i++) {
                tmp.push(decodeBase64toImg(imgs[i]))
            }
            setImages(tmp)
        }
    }, [imgs])

    const [index, setIndex] = useState(0);
    console.log(index);

    return (
        <div className="slide-container">
            <div className="slide-img-container">
                <img src={images[index]} />
                <button
                    style={{ left: 0 }}
                    onClick={() => {
                        if (index > 0) setIndex(index - 1);
                        else return;
                    }}
                >
                    &#10094;
                </button>
                <button
                    onClick={() => {
                        if (index < images.length - 1) setIndex(index + 1);
                        else return;
                    }}
                >
                    &#10095;
                </button>
            </div>
            {/* <div className="slide-dots">
        {imgs.map((item, i) => (
          <div
            className="slide-dot"
            key={i}
            onClick={() => {
              setIndex(i);
            }}
            style={{ backgroundColor: i === index ? "#717171" : "" }}
          />
        ))}
      </div> */}
        </div>
    );
};

export default SlideShow;
