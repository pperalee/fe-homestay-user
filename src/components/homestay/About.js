import React from 'react';
import {Link} from "react-router-dom";
import Button from "../shared/Button";

const AboutUs = () => {
    return (
        <section id="home" className="about-us">
            <div className="container">
                <div className="about-us-content">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="single-about-us">
                                <div className="about-us-txt">
                                    <h2>Jade Hill Homestay</h2>
                                    <p>
                                        Cùng bạn chinh phục những điểm đến mới lạ, hấp dẫn. Giúp bạn tận hưởng 
                                        và có những trải nghiệm tuyệt vời. 
                                        Đã có mặt tại nhiều thành phố lớn và khu du lịch nổi tiếng trên cả nước.
                                    </p>   
                                    <div className="about-btn">
                                        <a href="/#book">
                                            <Button className="about-view">Xem chi tiết</Button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-0">
                            <div className="single-about-us" />
                        </div>
                    </div>
                </div>
            </div>
        </section>

    )
}

export default AboutUs;
