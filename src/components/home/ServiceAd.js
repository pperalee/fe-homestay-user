import React from 'react';
import {Link} from "react-router-dom";

const ServiceAd = () => {
  return (
    <section className="service">
      <div className="container">
        <div className="service-counter text-center">
          <div className="col-md-4 col-sm-4">
            <div className="single-service-box">
              <div className="service-img">
                <img src={process.env.PUBLIC_URL + "/images/service/s1.png"}
                     alt="service-icon"/>
              </div>
              <div className="service-content">
                <h2>
                  <a href="/#booking"> thuận tiện </a>
                </h2>
                <p>
                  Dễ dàng đặt phòng với vài thao tác.
                </p>
              </div>
            </div>
          </div>
          {/*/.col*/}
          <div className="col-md-4 col-sm-4">
            <div className="single-service-box">
              <div className="service-img">
                <img src={process.env.PUBLIC_URL + "/images/service/s2.png"}
                     alt="service-icon"/>
              </div>
              <div className="service-content">
                <h2>
                  <a href="/#booking"> minh bạch </a>
                </h2>
                <p>
                  Giá phòng, giá dịch vụ được hiển thị chi tiết.
                </p>
              </div>
            </div>
          </div>
          <div className="col-md-4 col-sm-4">
            <div className="single-service-box">
              <div className="statistics-img">
                <img src={process.env.PUBLIC_URL + "/images/service/s3.png"}
                     alt="service-icon"/>
              </div>
              <div className="service-content">
                <h2>
                  <a href="/#booking"> thoải mái tận hưởng </a>
                </h2>
                <p>
                  Phòng ốc tiện nghi, sạch sẽ, nhiều dịch vụ đi kèm.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default ServiceAd;
