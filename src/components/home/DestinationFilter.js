import React, { Component } from 'react';
import axios from 'axios';
import { BookingBox } from './BookingBox';
import Homestay from '../homestay/Homestay.js';
import { Link } from "react-router-dom";

export class DestinationFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cities: [],
      homestays: [],
      showBookingBox: false,
      bookingHomestay: null,
      showHomestay: false,
    }

    this.onSearchChanged = this.onSearchChanged.bind(this);
    this.showBookingBox = this.showBookingBox.bind(this);
    this.showHomestay = this.showHomestay.bind(this);
    this.hideBookingBox = this.hideBookingBox.bind(this);
    this.hideHomestay = this.hideHomestay.bind(this)
  }

  componentDidMount() {
    axios
      .get(process.env.REACT_APP_API_URL + "search/cities")
      .then(response => {
        this.setState({
          cities: response.data
        })
      });

  }

  onSearchChanged(e) {
    console.log(e.target.value);
    axios
      .get(process.env.REACT_APP_API_URL + "search?cityid=" + e.target.value)
      .then(response => {
        console.log(response.data)
        this.setState({
          homestays: response.data
        })
      });
  }

  hideBookingBox() {
    this.setState({
      showBookingBox: false,
      bookingHomestay: null,
    })
  }

  showBookingBox(homestay) {
    console.log("Heheheh")
    this.setState({
      showBookingBox: true,
      bookingHomestay: homestay,
    })
  }

  showHomestay(homestay) {
    console.log("Hooooooo")
    this.setState({
      showHomestay: true,
      bookingHomestay: homestay,
    })
  }

  hideHomestay() {
    this.setState({
      showBookingBox: false,
      bookingHomestay: null,
    })
  }


  decodeBase64toImg(base64string, index) {
    let src = "data:image/jpeg;base64,"
    src += base64string;
    return src;
    // var newImage = document.createElement('img')
    // newImage.src = src
    // newImage.width = newImage.height = "200"
    // document.getElementById(`#imageContainer${index}`).innerHTML = newImage.outerHTML
  }


  render() {

    return (
      <section className="travel-box" id="explore">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="single-travel-boxes">
                <div id="desc-tabs" className="desc-tabs">
                  <div className="tab-content">
                    <div
                      role="tabpanel"
                      className="tab-pane active fade in"
                      id="tours"
                    >
                      <div className="tab-para">
                        <div className="row">
                          <div className="col-12">
                            <div className="single-tab-select-box">
                              <h2>Điểm đến</h2>

                              <div className="travel-select-icon">
                                <select className="form-control" onChange={this.onSearchChanged}>
                                  <option value="default">
                                    Chọn thành phố bạn muốn đến
                                  </option>
                                  {
                                    this.state.cities.map((city) => <option value={city._id}>{city.name}</option>)
                                  }
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                        {/* <div className="row">
                          <div className="col-sm-5"></div>
                          <div className="clo-sm-7">
                            <div className="about-btn travel-mrt-0 pull-right">
                              <button className="about-view travel-btn">
                                Tìm kiếm
                              </button>
                            </div>
                          </div>
                        </div> */}

                      </div>
                      <style>

                      </style>
                      <div>
                        {this.state.homestays.map((homestay, index) => (

                          <div className="travel-item blog-content">
                            <div
                              style={{
                                display: "flex",
                                alignItems: "center",
                                padding: "15px",
                                flexDirection: "column",
                                alignItems: "stretch",
                                justifyContent: "center",
                                boxShadow: "rgb(3 0 71 / 40%) 0px 1px 3px",
                                borderRadius: "5px"
                              }}
                            >
                              <div
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                  justifyContent: "flex-start",
                                  boxShadow: "rgb(3 0 71 / 40%) 0px 1px 3px",
                                  borderRadius: "2px"
                                }}>

                                {/* <div id={`imageContainer${index}`}> */}

                                {homestay.images[0] === 'image' ?
                                  <img
                                    src="https://cdn.tgdd.vn/Files/2020/12/22/1315425/homestay-la-gi--12.jpg"
                                    width="200px"
                                    height="200px"
                                    alt="hihi"
                                    id={`img${index}`}
                                  /> :
                                  <img
                                    src={this.decodeBase64toImg(homestay.images[0])}
                                    width="200px"
                                    height="200px"
                                    alt="hihi"
                                    id={`img${index}`}
                                  />
                                }
                                {/* </div> */}

                                {/* <div> */}
                                {/* <div className="blog-txt"> */}
                                <div style={{ margin: "5px 10px" }}>
                                  <h2>
                                    <b>{homestay.name}</b>
                                  </h2>
                                  <p>Địa chỉ: {homestay.address}</p>
                                  <br></br>
                                  <p>
                                    <i>{homestay.description}</i>
                                  </p>
                                </div>
                                {/* </div> */}

                              </div>

                              <div>
                                <div className="about-btn travel-mrt-0 pull-right" style={{ display: "flex" }}>
                                  <button style={{ marginRight: "20px" }} onClick={() => this.showBookingBox(homestay)} className="about-view packages-btn">
                                    Đặt phòng
                                  </button>
                                  <button onClick={() => this.showHomestay(homestay)} className="about-view packages-btn">
                                    Xem chi tiết
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        ))}

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {this.state.showBookingBox && this.state.bookingHomestay &&
          <BookingBox homestay={this.state.bookingHomestay} show={this.state.showBookingBox} onHide={this.hideBookingBox} />
        }
        {this.state.showHomestay && this.state.bookingHomestay &&
          <Homestay homestay={this.state.bookingHomestay} show={this.state.showHomestay} onHide={this.hideHomestay} />
        }
      </section>
    );
  }
}
