import React from "react";
import { BsPlus, BsDash } from "react-icons/bs";

class BookingItem extends React.Component {

  indexRoom(id) {
    const idxBooking = this.props.roomsBooking.findIndex((r) => r.id === id);
    const idx = this.props.rooms.findIndex((r) => r.id === id);
    return { idx, idxBooking };
  }

  decreaseHandle(idx, idxBooking) {
    const { item, roomsBooking, setRoomsBooking, rooms, setRooms, setSum, sum } = this.props;
    if (idxBooking < 0) return;
    if (roomsBooking[idxBooking].number >= 2) {
      let new_info_booking = [...roomsBooking];
      let new_info_all = [...rooms];
      new_info_booking[idxBooking].number -= 1;
      new_info_all[idx].number += 1;
      setRoomsBooking(new_info_booking);
      setRooms(new_info_all);
    } else {
      let new_info_booking = [...roomsBooking];
      let new_info_all = [...rooms];
      new_info_booking = new_info_booking.filter((i) => i.id !== item.id);
      new_info_all[idx].number += 1;
      setRoomsBooking(new_info_booking);
      setRooms(new_info_all);
    }
    const itemPrice = rooms.find(i => i.id === item.id).price
    setSum(sum - itemPrice)
  }

  increaseHandle(idx, idxBooking) {
    const { item, roomsBooking, setRoomsBooking, rooms, setRooms, setSum, sum } = this.props;

    if (rooms[idx].number >= 1) {
      if (idxBooking < 0) {
        let new_info_booking = [
          ...roomsBooking,
          { id: item.id, type: item.type, number: 1, price: item.price },
        ];
        let new_info_all = [...rooms];
        new_info_all[idx].number -= 1;
        setRoomsBooking(new_info_booking);
        setRooms(new_info_all);
      } else {
        let new_info_booking = [...roomsBooking];
        let new_info_all = [...rooms];
        new_info_booking[idxBooking].number += 1;
        new_info_all[idx].number -= 1;
        setRoomsBooking(new_info_booking);
        setRooms(new_info_all);
      }
      const itemPrice = rooms.find(i => i.id === item.id).price
      setSum(sum + itemPrice)
    } else return;
  }

  render() {
    const { item, rooms, } = this.props;
    const { idx, idxBooking } = this.indexRoom(item.id);
    console.log(this.props.roomsBooking)
    console.log(idx, idxBooking)
    return (
      <>
        <p>
          {item.type} - So phong con lai: {rooms[idx].number}
        </p>
        <div className="booking-item-symbols">
          <BsDash
            className="booking-item-symbols-item"
            size={24}
            onClick={() => {
              this.decreaseHandle(idx, idxBooking)
            }}
          />
          <p>{this.props.roomsBooking[idxBooking] ? this.props.roomsBooking[idxBooking].number : 0}</p>
          <BsPlus
            className="booking-item-symbols-item"
            size={24}
            onClick={() => {
              this.increaseHandle(idx, idxBooking)
            }}
          />
        </div>
      </>
    );
  };
}

export default BookingItem;
