import React, { Component, useEffect } from 'react';
import Button from '../shared/Button';
import { Modal } from 'react-bootstrap';
import BookingItem from './BookingItem';
import axios from 'axios';


export class BookingBox extends Component {
  constructor(props) {
    super(props);

    // const allRooms = [
    //   { id: 1, type: "Loai 1", number: 3, price: 100000 },
    //   { id: 2, type: "Loai 2", number: 4, price: 120000 },
    //   { id: 3, type: "Loai 3", number: 5, price: 200000 },
    // ];

    this.state = {
      roomsBooking: [],
      rooms: [],
      book_from: "",
      book_to: "",
      persons: 0,
      sum: 0,
      email: "",
      phone: "",
      name: "",
      message: ""
    }

    this.setRoomsBooking = this.setRoomsBooking.bind(this);
    this.setRooms = this.setRooms.bind(this);
    this.setBookFrom = this.setBookFrom.bind(this)
    this.setBookTo = this.setBookTo.bind(this)
    this.setPersons = this.setPersons.bind(this)
    this.setSum = this.setSum.bind(this)
    // this.componentDidMount = this.componentDidMount.bind(this)
    this.sendBooking = this.sendBooking.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.setEmail = this.setEmail.bind(this)
    this.setName = this.setName.bind(this)
    this.setPhone = this.setPhone.bind(this)
    this.filterRooms = this.filterRooms.bind(this)
    this.setMessage = this.setMessage.bind(this)
  }

  componentDidMount() {
    const { homestay } = this.props;
    axios
      .get(process.env.REACT_APP_API_URL + `homestays/${homestay._id}/roomTypes`)
      .then(response => {
        const allRooms = response.data.map(item => {
          return { id: item._id, type: item.name, number: item.total_rooms, price: item.price }
        });
        this.setState({
          rooms: allRooms
        })
      });
  }

  setMessage(){
    this.setState({
      message: true
    })
  }

  filterRooms(e) {
    e.preventDefault()
    const { homestay } = this.props;
    axios
      .get(process.env.REACT_APP_API_URL +
        `homestays/${homestay._id}/roomTypes?start=${this.state.book_from}&end=${this.state.book_to}`)
      .then(response => {
        console.log(response.data)
        // const allRooms = response.data.map(item => {
        //   return { id: item._id, type: item.name, number: item.total_rooms, price: item.price }
        // });
        // this.setState({
        //   rooms: allRooms
        // })
      });
  }

  sendBooking() {
    let books = []
    if (this.state.roomsBooking.length <= 0) return;
    books = this.state.roomsBooking.map(item => {
      return { room_type: item.id, volume: item.number }
    })
    const booking = {
      checkin: this.state.book_from,
      checkout: this.state.book_to,
      guests: + this.state.persons,
      books: books,
      user_info: {
        email: this.state.email,
        phone: this.state.phone,
        name: this.state.name
      },
      total: this.state.sum,

    }
    axios
      .post(process.env.REACT_APP_API_URL + "booking", booking)
      .then(response => {
        console.log(response)
      });
    this.setMessage()
  }

  handleSubmit(e) {
    e.preventDefault()
    this.sendBooking()

  }

  // useEffect(updateSum () {
  //   if (this.state.roomsBooking.length < 0) {
  //     this.setState({
  //       sum: 0
  //     })
  //   } else {
  //     // console.log(this.state.roomsBooking)
  //     this.setState({
  //       sum: this.state.roomsBooking.map(item => item.number * item.price)
  //     })
  //   }
  // }, [this.state.roomsBooking])

  setName(value) {
    this.setState({
      name: value
    })
  }

  setPhone(value) {
    this.setState({
      phone: value
    })
  }

  setEmail(value) {
    this.setState({
      email: value
    })
  }

  setSum(value) {
    this.setState({
      sum: value
    })
  }


  setRoomsBooking(roomsBooking) {
    this.setState({
      roomsBooking: roomsBooking
    })
  }

  setRooms(rooms) {
    this.setState({
      rooms: rooms
    })
  }

  setBookFrom(time) {
    this.setState({
      book_from: time
    })
  }

  setBookTo(time) {
    this.setState({
      book_to: time
    })
  }

  setPersons(amount) {
    this.setState({
      persons: amount
    })
  }


  render() {
    return (
      <Modal show={this.props.show} bsSize="lg" onHide={this.props.onHide}>
        <Modal.Header closeButton>
          <Modal.Title>{this.props.homestay.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="booking-container">
            <div className="booking-form-filter">
              <form className="booking-form-filter-form" onSubmit={this.filterRooms}>
                <input type="text" placeholder="Number of people" onChange={(e) => { this.setPersons(e.target.value) }} />
                <label htmlFor="check-in">Ngày đến</label>
                <input type="date" id="check-in" onChange={(e) => { this.setBookFrom(e.target.value) }} />
                <label htmlFor="check-out">Ngày đi</label>
                <input type="date" id="check-out" onChange={(e) => { this.setBookTo(e.target.value) }} />
                <button type="submit">
                  Kiểm tra phòng trống
                </button>
              </form>
            </div>
            <div className="booking-show">
              <div className="booking-list">
                <h2>List Rooms</h2>
                <div className="booking-item-container">
                  <ul className="booking-item-list">
                    {this.state.rooms.map((item) => (
                      <li key={Math.random().toString()}>
                        <BookingItem
                          item={item}
                          roomsBooking={this.state.roomsBooking}
                          setRoomsBooking={this.setRoomsBooking}
                          rooms={this.state.rooms}
                          setRooms={this.setRooms}
                          setSum={this.setSum}
                          sum={this.state.sum}
                        />
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
              <div className="booking-form-reservation">
                <h2>Reservation Form</h2>
                <form className="booking-form" onSubmit={this.handleSubmit}>
                  <label htmlFor="user-name">Họ và Tên</label>
                  <input type="text" required id="user-name" onChange={(e) => { this.setName(e.target.value) }} />
                  <label htmlFor="user-phone">Số điện thoại</label>
                  <input type="text" required id="phone" onChange={(e) => { this.setPhone(e.target.value) }} />
                  <label htmlFor="user-email">Email</label>
                  <input type="email" required id="user-email" onChange={(e) => { this.setEmail(e.target.value) }} />
                  <p><b>Thành tiền:</b> {this.state.sum} VND</p>
                  {/* <div className="booking-form-table-container"> */}
                  {/* <table className="booking-form-table">
                      <tr className="booking-form-table-row">
                        <th className="booking-form-table-header">Loại phòng</th>
                        <th className="booking-form-table-header">Số lượng</th>
                      </tr>
                      {this.state.roomsBooking.map((item) => (
                        <tr
                          className="booking-form-table-row"
                          key={Math.random().toString()}
                        >
                          <td className="booking-form-table-data">{item.type}</td>
                          <td className="booking-form-table-data">{item.number}</td>
                        </tr>
                      ))}
                    </table> */}
                  {/* </div> */}
                  <button type="submit">Đặt phòng</button>
                </form>
                {this.state.message && <p style={{fontSize: "20px", marginTop: "7px"}}><b><i>Đặt phòng thành công</i></b></p>}
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    )
  }
}
