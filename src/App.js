import React, { useEffect, useState, useRef } from "react";
import { Route, useLocation } from "react-router";
import { Layout } from "./components/Layout";
import Home from "./components/home/Home";


const App = () => {
  const location = window.location;
  useEffect(() => {
    if (location.hash) {
      const yOffset = 0;
      const element = document.getElementById(location.hash.slice(1));
      if (element) {
        const y =
          element.getBoundingClientRect().top + window.pageYOffset + yOffset;
        window.scrollTo({ top: y, behavior: "smooth" });
        return;
      }
    }
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  }, [location.hash]);


  return (
    <Layout>
      <Home />
    </Layout>
  );
};

export default App;
